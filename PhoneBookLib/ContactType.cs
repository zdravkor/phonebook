﻿namespace PhoneBookLib
{
    public static class ContactType
    {
        public static string Work = "Work";
        public static string Cellphone = "Cell";
        public static string Home = "Home";
    }
}