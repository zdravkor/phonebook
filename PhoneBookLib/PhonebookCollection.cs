﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;


namespace PhoneBookLib
{
    [Serializable]
    public class PhoneBookCollection
    {
        public List<PhonebookEntry> phonebookEntries;

        public PhoneBookCollection()
        {
            phonebookEntries = new List<PhonebookEntry>();
        }

        public bool Add(string firstName, string lastName, string contactType, string phoneNumber)
        {
            var entry = new PhonebookEntry
            {
                Id = Guid.NewGuid().ToString(),
                FirstName =  firstName,
                LastName =  lastName,
                ContactType = contactType,
                PhoneNumber = phoneNumber
            };

            if (!EntryIsValid(entry))
                return false;

            phonebookEntries.Add(entry);
            return true;
        }

        public bool Edit(string entryId, string firstName, string lastName, string contactType, string phoneNumber)
        {
            var entry = new PhonebookEntry
            {
                FirstName = firstName,
                LastName = lastName,
                ContactType = contactType,
                PhoneNumber = phoneNumber
            };

            if (!EntryIsValid(entry))
                return false;

            var en = phonebookEntries.FirstOrDefault(e => e.Id == entryId);
            if(en == null)
                return false;

            en.FirstName = entry.FirstName;
            en.LastName = entry.LastName;
            en.ContactType = entry.ContactType;
            en.PhoneNumber = entry.PhoneNumber;
            return true;
        }

        public bool Delete(string id)
        {
            var en = phonebookEntries.FirstOrDefault(e => e.Id == id);
            if (en == null)
                return false;

            phonebookEntries.Remove(en);
            return true;
        }

        public void DeleteAllEntries()
        {
            phonebookEntries.Clear();
        }

        public string GetFirstEntryGuid()
        {
            return phonebookEntries.FirstOrDefault().Id;
        }

        private bool EntryIsValid(PhonebookEntry entry)
        {
            //all fields are mandatory
            if (string.IsNullOrWhiteSpace(entry.FirstName) ||
                string.IsNullOrWhiteSpace(entry.LastName) ||
                string.IsNullOrWhiteSpace(entry.ContactType) ||
                string.IsNullOrWhiteSpace(entry.PhoneNumber))
                return false;

            return true;
        }
    }
}
