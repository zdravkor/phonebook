﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBookLib
{
    [Serializable]
    public class PhonebookEntry
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactType { get; set; }
        public string PhoneNumber { get; set; }
    }
}
