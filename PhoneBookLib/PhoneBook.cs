﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace PhoneBookLib
{
    public class PhoneBook
    {
        public PhoneBookCollection collection;
        private readonly PhonebookPersistence persistence = new PhonebookPersistence();

        public PhoneBook()
        {
            collection = persistence.DeserializePhonebook();
        }

        public List<PhonebookEntry> GetPhonebookEntries(string searchFirstName = null, string searchLastName = null)
        {
            var retCollection = new List<PhonebookEntry>();
            retCollection = collection.phonebookEntries.ToList();

            if (!string.IsNullOrWhiteSpace(searchFirstName))
                retCollection = retCollection.Where(e => e.FirstName.ToLower().Contains(searchFirstName.ToLower())).ToList();

            if (!string.IsNullOrWhiteSpace(searchLastName))
                retCollection = retCollection.Where(e => e.LastName.ToLower().Contains(searchLastName.ToLower())).ToList();

            return retCollection;
        }

        public void Add(string firstName, string lastName, string type, string phoneNumber)
        {
            if (collection.Add(firstName, lastName, type, phoneNumber))
            {
                //if adding was successful, save
                persistence.SerializePhonebook(collection);
            }
        }

        public void Edit(string entryId, string firstName, string lastName, string contactType, string phoneNumber)
        {
            if (collection.Edit(entryId, firstName, lastName, contactType, phoneNumber))
            {
                //if edit was successful, save
                persistence.SerializePhonebook(collection);
            }
        }

        public void Search(string firstName, string lastName)
        {
            
        }

        public void Delete(string entryId)
        {
            if (collection.Delete(entryId))
            {
                //if delete was successful, save
                persistence.SerializePhonebook(collection);
            }
        }

        public void DeleteAllEntries()
        {
            collection.DeleteAllEntries();
            persistence.SerializePhonebook(collection);
        }

        public bool IsEmpty()
        {
            if (collection.phonebookEntries.Any())
                return false;

            return true;
        }

    }
}
