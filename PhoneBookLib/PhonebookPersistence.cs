﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace PhoneBookLib
{
    public class PhonebookPersistence
    {
        private const string fileName = "Phonebook.dat";
        
        public void SerializePhonebook(PhoneBookCollection collection)
        {
            var formatter = new BinaryFormatter();
            using (var fileStream = new FileStream(fileName, FileMode.OpenOrCreate))
            {
                formatter.Serialize(fileStream, collection);
            }
        }

        public PhoneBookCollection DeserializePhonebook()
        {
            var formatter = new BinaryFormatter();
            using (var fileStream = new FileStream(fileName, FileMode.OpenOrCreate))
            {
                if (fileStream.Length > 0)
                    return (PhoneBookCollection)formatter.Deserialize(fileStream);
                else
                    return new PhoneBookCollection(); //empty file, return empty list
            }
        }
    }
}
