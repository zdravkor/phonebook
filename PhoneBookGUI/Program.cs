﻿using PhoneBookLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PhoneBookGUI
{
    class Program
    {
        static void Main(string[] args)
        {
            var phonebook = new PhoneBook();

            Console.WriteLine(">Phonebook Manager v1.0");
            Console.WriteLine();
            Console.WriteLine(">Emptying phonebook...");
            phonebook.DeleteAllEntries();
            DisplayPhonebook(phonebook);

            //Create
            AddDummyContacts(phonebook);
            DisplayPhonebook(phonebook);

            //Edit
            Console.WriteLine();
            Console.WriteLine(">EDIT: Editing first entry, changing name from Zdravko to NEBOJSA");
            var firstEntryGuid = phonebook.collection.GetFirstEntryGuid();
            phonebook.Edit(firstEntryGuid, "NEBOJSA", "Radmanovic", ContactType.Cellphone, "0601500708");
            DisplayPhonebook(phonebook);

            //Delete
            Console.WriteLine();
            Console.WriteLine(">DELETE: Delete first entry");
            phonebook.Delete(firstEntryGuid);
            DisplayPhonebook(phonebook);

            //Search first name
            Console.WriteLine();
            Console.WriteLine(">SEARCH: First name contains 'j'");
            DisplayPhonebook(phonebook, "j");

            //Search last name
            Console.WriteLine();
            Console.WriteLine(">SEARCH: Last name contains 'k'");
            DisplayPhonebook(phonebook, null, "k");

            //Search first and last name
            Console.WriteLine();
            Console.WriteLine(">SEARCH: First name contains 's' and last name contains 'i'");
            DisplayPhonebook(phonebook, "s", "i");

            Console.ReadLine();
        }

        private static void DisplayHeader()
        {
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("ID \t\t\t\t\t First Name \t Last Name \t Type \t Phone");
            Console.ResetColor();
        }

        private static void DisplayPhonebook(PhoneBook phonebook, string searchFirstName = null, string searchLastName = null)
        {

            if (!phonebook.IsEmpty())
            {
                DisplayHeader();
                foreach (var entry in phonebook.GetPhonebookEntries(searchFirstName, searchLastName))
                {
                    Console.WriteLine("{0} \t {1} \t {2} \t {3} \t {4}", entry.Id, entry.FirstName, entry.LastName,
                        entry.ContactType, entry.PhoneNumber);
                }
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine(">Phonebook is empty!");
            }
        }

        private static void AddDummyContacts(PhoneBook phonebook)
        {
            Console.WriteLine();
            Console.WriteLine(">CREATE: Adding five dummy contacts...");
            phonebook.Add("Zdravko", "Radmanovic", ContactType.Cellphone, "0601500708");
            phonebook.Add("Nemanja", "Ivanovic", ContactType.Home, "0112610754");
            phonebook.Add("Adrijana", "Radmanovic", ContactType.Work, "01112324567");
            phonebook.Add("Sandra", "Markovic", ContactType.Home, "0113216547");
            phonebook.Add("Dalibor", "Nikolic", ContactType.Cellphone, "0116549875");
            Console.WriteLine(">Five dummy contacts added!");
            Console.WriteLine();
        }
    }
}
